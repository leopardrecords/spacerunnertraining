using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{

    [SerializeField] private float scrollSpeed = -2f;

    private float width;
    private Vector2 resetPosition;
    private BoxCollider2D collider;
    private Rigidbody2D rb;

    void Start()
    {
        collider = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();

        width = collider.size.x;
        resetPosition = new Vector2(width * 2f, 0);
        collider.enabled = false;

        rb.velocity = new Vector2(scrollSpeed, 0);
    }

    void Update()
    {
        if (transform.position.x < -width)
        {
            transform.position = (Vector2)transform.position + resetPosition;
        }
    }
}
