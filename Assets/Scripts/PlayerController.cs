using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
   [SerializeField] private float moveSpeed = 5;

    private Rigidbody2D rb;
    private float moveDirection;

    private const string vertical = "Vertical";
    private const string obstacleTag = "Obstacle";

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        moveDirection = Input.GetAxisRaw(vertical);
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(0, moveDirection * moveSpeed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
     if (other.gameObject.CompareTag(obstacleTag))
        {
            Destroy(gameObject);
        }
    }
}
