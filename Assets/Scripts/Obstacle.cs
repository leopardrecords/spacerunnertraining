using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private Vector2 xPosRange;
    [SerializeField] private Vector2 yPosRange;
    [SerializeField] private Vector2 xVelRange;
    [SerializeField] private Vector2 yVelRange;
    private Vector3 velocity;
    private float xDestroyPosition;
    
    private void Update()
    {
        Move();
    }

    public void Init(float xDestroyPosition, Vector3 scale)
    {
        this.xDestroyPosition = xDestroyPosition;
        transform.localScale = scale;
        ResetObstacle();
    }

    private void ResetObstacle()
    {
        velocity = new Vector3(Random.Range(xVelRange.x, xVelRange.y), Random.Range(yVelRange.x, yVelRange.y), 0);
        transform.position = new Vector3(Random.Range(xPosRange.x, xPosRange.y), Random.Range(yPosRange.x, yPosRange.y), 0);
    }

    private void Move()
    {
        transform.Translate(-velocity * Time.deltaTime);
        if (transform.position.x < xDestroyPosition)
        {
            ResetObstacle();
        }
    }

}
