using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    [SerializeField] private Transform obstacleResetPoint;
    [SerializeField] private Obstacle obstaclePrefab;
    [SerializeField] private Vector2 obstacleScaleRange;
    [SerializeField] private int obstaclesAmount;

    void Start()
    {
        for (int i = 0; i < obstaclesAmount; i++)
        {
            Obstacle obstacle = Instantiate(obstaclePrefab);
            Vector3 obstacleScale = new Vector3(Random.Range(obstacleScaleRange.x, obstacleScaleRange.y), Random.Range(obstacleScaleRange.x, obstacleScaleRange.y), 1);
            obstacle.Init(obstacleResetPoint.position.x, obstacleScale);
        }
    }
}
